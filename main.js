var direction = new Direction();
direction.ChangeScene(0);

canvas.onmousemove = function(e) {
    direction.MouseMoveEvent(e);
}

canvas.onmousedown = function(e) {
    direction.MouseDownEvent(e);
}

canvas.onmouseup = function(e) {
    direction.MouseUpEvent(e);
}

document.addEventListener("keydown", function(e) {
    direction.KeyDownEvent(e);
}, false);

document.addEventListener("keyup", function(e) {
    direction.KeyUpEvent(e);
}, false);

setInterval(function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    direction.Update();
    direction.Draw();
}, 10);