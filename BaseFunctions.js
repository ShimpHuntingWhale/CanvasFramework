// Random
function Random(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// get mouse position
function getMousePos(e) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };
}

// Rect to Rect Collision
function R2RCollision(r1, r2) {
    return (r1.top < r2.bottom && r1.bottom > r2.top && r1.left < r2.right && r1.right > r2.left );
}

// Circle to Circle Collision
function C2CCollision(c1, c2) {
    var d = Math.sqrt( Math.pow(c1.pos.x - c2.pos.x, 2) + Math.pow(c1.pos.y - c2.pos.y, 2) );
    return (d <= c1.radius + c2.radius);
}