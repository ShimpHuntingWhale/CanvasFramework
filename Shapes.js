// init Canvas
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var pause = true;

// BaseShapeInfo Class
function BaseShapeInfo() {
    this.pos = { x: 0, y: 0 }; this.centerPos = { x: 0, y: 0 };
    this.size = { width: 0, height: 0 };
    this.setBoundingBox();
    this.color = "black";

    this.isStroke = false;
    this.strokeSize = 0;
    this.strokeColor = "black";

    this.isShadow = false;
    this.shadowColor = "#999";
    this.shadowBlur = 0;
    this.shadowPos = { x: 0, y: 0 };

    this.isDrawBoundingBox = false;
    this.boundingBoxColor = "blue";
    this.boundingBoxLineWidth = 1;
}
BaseShapeInfo.prototype.setBoundingBox = function() {
    this.boundingBox = {
        left: this.pos.x - this.size.width * this.centerPos.x, 
        right: this.pos.x + this.size.width * (1.0 - this.centerPos.x),
        top: this.pos.y - this.size.height * this.centerPos.y, 
        bottom: this.pos.y + this.size.height * (1.0 - this.centerPos.y)
    };
}
BaseShapeInfo.prototype.drawBoundingBox = function() {
    ctx.beginPath();
    ctx.rect(
        this.boundingBox.left, this.boundingBox.top,
        this.boundingBox.right - this.boundingBox.left, 
        this.boundingBox.bottom - this.boundingBox.top
    );
    ctx.strokeStyle = this.boundingBoxColor;
    ctx.lineWidth = this.boundingBoxLineWidth;
    ctx.stroke();
    ctx.closePath();
}
BaseShapeInfo.prototype.setSize = function(w, h) {
    this.size.width = w; this.size.height = h;
    this.setBoundingBox();
}
BaseShapeInfo.prototype.setPosition = function(x, y) {
    this.pos.x = x; this.pos.y = y;
    this.setBoundingBox();
}
BaseShapeInfo.prototype.setPositionX = function(x) {
    this.pos.x = x;
    this.setBoundingBox();
}
BaseShapeInfo.prototype.setPositionY = function(y) {
    this.pos.y = y;
    this.setBoundingBox();
}
BaseShapeInfo.prototype.setCenterPos = function(x, y) {
    if(x < 0 || x > 1 || y < 0 || y > 1) { return null; }
    this.centerPos.x = x; this.centerPos.y = y;
    this.setBoundingBox();
}
BaseShapeInfo.prototype.setStroke = function(isStroke, size=0, color="black") {
    this.isStroke = isStroke;
    this.strokeSize = size;
    this.strokeColor = color;
}
BaseShapeInfo.prototype.setShadow = function(isShadow, x=0, y=0, blur=0) {
    this.isShadow = isShadow;
    this.shadowPos.x = x; this.shadowPos.y = y;
    this.shadowBlur = blur;
}

// Rect Class
function Rect(width, height, color="black", pos={x:0, y:0}, centerPos={x:0, y:0}) {
    BaseShapeInfo.call(this);

    this.pos = pos; this.centerPos = centerPos;
    this.size.width = width; this.size.height = height;
    this.setBoundingBox();
    this.color = color;
}
Rect.prototype = new BaseShapeInfo();
Rect.prototype.constructor = Rect;
Rect.prototype.Draw = function() {
    ctx.beginPath();
    ctx.rect(this.boundingBox.left, this.boundingBox.top, this.size.width, this.size.height);
    if(this.isShadow) {
        ctx.shadowOffsetX = this.shadowPos.x;
        ctx.shadowOffsetY = this.shadowPos.y;
        ctx.shadowColor = this.shadowColor;
        ctx.shadowBlur = this.shadowBlur;
    }
    ctx.fillStyle = this.color;
    ctx.fill();
    if(this.isStroke) {
        ctx.strokeStyle = this.strokeColor;
        ctx.lineWidth = this.strokeSize;
        ctx.stroke();
    }
    ctx.closePath();

    if(this.isShadow) {
        ctx.shadowBlur = 0;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
    }
    if(this.isDrawBoundingBox) { this.drawBoundingBox(); }
}

// Circle Class
function Circle(radius, color="black", pos={x:0, y:0}, centerPos={x:0, y:0}){
    BaseShapeInfo.call(this);

    this.pos = pos; this.centerPos = centerPos;
    this.radius = radius; this.color = color;
    this.size = { width: radius * 2, height: radius * 2 };
    this.setBoundingBox();
}
Circle.prototype = new BaseShapeInfo();
Circle.prototype.constructor = Circle;
Circle.prototype.setRadius = function(radius) {
    this.radius = radius;
    this.size = { width: radius * 2, height: radius * 2 };
    this.setBoundingBox();
}
Circle.prototype.Draw = function() {
    ctx.beginPath();
    ctx.arc(
        this.boundingBox.left + this.radius, 
        this.boundingBox.top + this.radius, 
        this.radius, 0, Math.PI * 2, true
    );
    if(this.isShadow) {
        ctx.shadowOffsetX = this.shadowPos.x;
        ctx.shadowOffsetY = this.shadowPos.y;
        ctx.shadowColor = this.shadowColor;
        ctx.shadowBlur = this.shadowBlur;
    }
    ctx.fillStyle = this.color;
    ctx.fill();
    if(this.isStroke) {
        ctx.strokeStyle = this.strokeColor;
        ctx.lineWidth = this.strokeSize;
        ctx.stroke();
    }
    ctx.closePath();

    if(this.isShadow) {
        ctx.shadowBlur = 0;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
    }
    if(this.isDrawBoundingBox) { this.drawBoundingBox(); }
}

// Triangle Class
function Triangle(radius, color="black", pos={x:0,y:0}, centerPos={x:0,y:0}) {
    Circle.call(this);

    this.pos = pos; this.centerPos = centerPos;
    this.radius = radius; this.color = color;
    this.size = { width: radius * 2, height: radius * 2 };
    this.setBoundingBox();

    this.vector = new Array(3);
    this.setVector(90);
    this.setVertex();
}
Triangle.prototype = new Circle(0);
Triangle.prototype.constructor = Triangle;
Triangle.prototype.getRadian = function(theta) {
    return theta * Math.PI / 180;
}
Triangle.prototype.setVector = function(theta) {
    this.startTheta = theta;
    for(var i = 0; i < 3; i++) {
        this.vector[i] = {
            x: Math.cos(this.getRadian(this.startTheta + 120 * i)),
            y: Math.sin(this.getRadian(this.startTheta + 120 * i))
        };
    }
    this.setVertex();
}
Triangle.prototype.setVertex = function() {
    const tmpX = this.boundingBox.left + this.radius;
    const tmpY = this.boundingBox.top + this.radius;
    this.vertex = [
        { x: tmpX + this.vector[0].x * this.radius, y: tmpY + this.vector[0].y * this.radius }, 
        { x: tmpX + this.vector[1].x * this.radius, y: tmpY + this.vector[1].y * this.radius }, 
        { x: tmpX + this.vector[2].x * this.radius, y: tmpY + this.vector[2].y * this.radius }
    ];
}
Triangle.prototype.setPosition = function(x, y) {
    this.pos.x = x; this.pos.y = y;
    this.setBoundingBox();
    this.setVertex();
}
Triangle.prototype.Draw = function() {
    ctx.beginPath();
    
    ctx.moveTo(this.vertex[0].x, this.vertex[0].y);
    ctx.lineTo(this.vertex[1].x, this.vertex[1].y);
    ctx.lineTo(this.vertex[2].x, this.vertex[2].y);

    if(this.isShadow) {
        ctx.shadowOffsetX = this.shadowPos.x;
        ctx.shadowOffsetY = this.shadowPos.y;
        ctx.shadowColor = this.shadowColor;
        ctx.shadowBlur = this.shadowBlur;
    }
    ctx.fillStyle = this.color;
    ctx.fill();
    if(this.isStroke) {
        ctx.strokeStyle = this.strokeColor;
        ctx.lineWidth = this.strokeSize;
        ctx.stroke();
    }
    ctx.closePath();

    if(this.isShadow) {
        ctx.shadowBlur = 0;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
    }
    if(this.isDrawBoundingBox) { this.drawBoundingBox(); }
}


// Font Class
function Font(fontName="Arial Black", size=15, color="black") {
    this.fontName = fontName; this.size = size;
    this.color = color;
}
Font.prototype.Draw = function(message, x, y, align) {
    ctx.font = this.size + "px " + this.fontName;
    ctx.fillStyle = this.color;
    ctx.textAlign = align;
    ctx.fillText(message, x, y);
}