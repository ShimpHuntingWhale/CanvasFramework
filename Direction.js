// Direction Class
function Direction() {
    this.scene;

    this.scenes = [
        new GameScene()
    ];
}
Direction.prototype.ChangeScene = function(index) {
    if(index >= 0 && index <= this.scenes.length - 1) { 
        this.scene = this.scenes[index];
        this.scene.Init();
    }
}
Direction.prototype.KeyDownEvent = function(e) {
    this.scene.KeyDownEvent(e);
}
Direction.prototype.KeyUpEvent = function(e) {
    this.scene.KeyUpEvent(e);
}
Direction.prototype.MouseDownEvent = function(e) {
    // this.scene.MouseDownEvent(e);
}
Direction.prototype.MouseUpEvent = function(e) {
    // this.scene.MouseUpEvent(e);
}
Direction.prototype.MouseMoveEvent = function(e) {
    // this.scene.MouseMoveEvent(e);
}
Direction.prototype.Draw = function() {
    this.scene.Draw();
}
Direction.prototype.Update = function() {
    // return -1 : None
    // return 0 ~ scenes.lengh : ChangeScene
    this.ChangeScene(this.scene.Update());
}